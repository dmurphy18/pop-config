types = (int, float, str, list, set, tuple)

CLI_CONFIG = {t.__name__: {} for t in types}

CONFIG = {t.__name__: {"dyne": "d1", "default": None, "type": t} for t in types}

DYNE = {"d1": ["d1"]}
